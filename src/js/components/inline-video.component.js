"use strict";

// -------------------------------------
//   Dependencies
// -------------------------------------
/** 
  * @plugins
**/

// base
require("../base/raf");
require("../base/event");
require("../base/query");

// -------------------------------------
//   Component - InlineVideo
// -------------------------------------
/** 
  * @name inline-video.component
  * @desc The inline-video component for the app.
**/

(function() {
    var CONFIG = require("../inline-video.config"); // config
    console.log("components/inline-video.component.js loaded.");

    /** 
      * @name InlineVideo
      * @desc Class for inline video component.
      * @param {Object} options - options for the component
      * @return {Object} - the instance of the the component class
    **/
    function InlineVideo(options) {
        // ---------------------------------------------
        //   Private members
        // ---------------------------------------------
        var _el = {  // reference to the various DOM elements
            main: null, // for main component DOM element

            image: {  // for the image elements used for component layout
                desktop: null, // component layout image for desktop
                tablet:  null, // component tablet image for tablet
                mobile:  null  // component mobile image for mobile
            },

            layout: null, // for the parent layout wrapper element
            canvas: null, // for the whitewater canvas element 
            video: null,  // for the default video element
            gif: null     // for the fallback gif element
        }

        var _class = { // reference to the various applied classes
            main: "inline-video", // for main component DOM element

            image: { // for the image elements used in the component
                default: "inline-video__image", // the default class
                desktop: "inline-video__image--desktop", // desktop
                tablet:  "inline-video__image--tablet", // tablet
                mobile:  "inline-video__image--mobile" // mobile
            },

            layout: "inline-video__layout", // for the parent layout wrapper element
            canvas: "inline-video__canvas", // for the whitewater canvas element 
            video: "inline-video__video",  // for the default video element
            gif: "inline-video__gif"     // for the fallback gif element
        };

        var _whitewater = { // reference to the whitewater video
            player: null, // reference to the whitewater player
            options: { loop: true }, // the whitewater options

            isLoading: false, // flag to indicate whitewater is loading
            hasLoaded: false  // flag to indicate whitewater had loaded
        };

        var _isReady   = false; // flag to indicate if the video is ready
        var _isLocked  = false; // flag to indicate if the video is locked
        var _isPlaying = false; // flag to indicate if the video is playing
        var _hasLoaded = false; // flag to indicate if the video has loaded

        var _removeVideoOnStop = false;  // flag to indicate if the video should be removed from the DOM when stopped
        var _removeCanvasOnStop = false; // flag to indicate if the canvas should be removed from the DOM when stopped

        // ---------------------------------------------
        //   Public members
        // ---------------------------------------------
        /* empty block */

        // ---------------------------------------------
        //   Private methods
        // ---------------------------------------------
        // @name _createLayout
        // @desc function to create a <div> layout wrapper for the component
        // @return {DOM} - the created device specific <div> layout element
        function _createLayout() {
            // create the layout element for the component
            var layout = document.createElement("div");

            // add the default layout classes
            layout.classList.add(_class.layout);

            // return the created element
            return layout;
        }

        // @name _createImage
        // @desc function to create a <img> element for the component
        // @param {String} key - the device specific key for the element
        // @return {DOM} - the created device specific <img> element
        function _createImage(key) {
            // create the image element for the component
            var image = document.createElement("img");

            // set the image attributes
            image.setAttribute("alt", "");
            image.setAttribute("src", options.source[key] + "layout.png");

            // set the first frame as the background image
            image.style.backgroundImage = "url('" + options.source[key] + "first.jpg')";

            // add the default and device specific image classes
            image.classList.add(_class.image.default);
            image.classList.add(_class.image[key]);

            // return the created element
            return image;
        }

        // @name _createVideo
        // @desc function to create a <video> element for the component
        // @param {String} key - the device specific key for the element
        // @return {DOM} - the created <video> element for the component
        function _createVideo(key) {
            // create the video element for the component
            var video = document.createElement("video");

            // set the video attributes
            video.setAttribute("alt", "");
            video.setAttribute("loop", "");
            video.setAttribute("muted", "");
            video.setAttribute("data-src", options.source[key] + "video.mp4");

            // set the first frame as the background image and poster image
            video.style.backgroundImage = "url('" + options.source[key] + "first.jpg')";
            video.setAttribute("poster", options.source[key] + "first.jpg");
            
            // add the default video classes
            video.classList.add(_class.video);

            // return the created element
            return video;
        }

        // @name _createCanvas
        // @desc function to create a <canvas> element for the component
        // @param {String} key - the device specific key for the element
        // @return {DOM} - the created <canvas> element for the component
        function _createCanvas(key) {
            // create the canvas element for the component
            var canvas = document.createElement("canvas");

            // set the canvas attributes
            canvas.setAttribute("alt", "");
            canvas.setAttribute("data-src", options.source[key]);

            // set the first frame as the background image
            canvas.style.backgroundImage = "url('" + options.source[key] + "first.jpg')";

            // add the default canvas classes
            canvas.classList.add(_class.canvas);

            // return the created element
            return canvas;
        }

        // @name _createGif
        // @desc function to create a <img> gif element for the component
        // @param {String} key - the device specific key for the element
        // @return {DOM} - the created <img> gif element for the component
        function _createGif(key) {
            // create the gif element for the component
            var gif = document.createElement("img");

            // set the gif attributes
            gif.setAttribute("alt", "");
            gif.setAttribute("data-src", options.source[key] + "video.gif");

            // set the first frame as the background image
            gif.style.backgroundImage = "url('" + options.source[key] + "first.jpg')";

            // add the default gif classes
            gif.classList.add(_class.gif);

            // return the created element
            return gif;
        }

        // @name _onInlineVideoReady
        // @desc listener function for on ready event for the inline video
        function _onInlineVideoReady() {
            _isReady = true; // set the video ready flag and create a ready event
            var event = new CustomEvent("inline-video.onReady", { detail: null });
            _el.main.dispatchEvent(event); // dispatch created event on the main element
        }

        // @name _onInlineVideoLoad
        // @desc listener function for on load event for the inline video
        function _onInlineVideoLoad() {
            _hasLoaded = true; // set the video load flag and create a load event
            var event = new CustomEvent("inline-video.onLoad", { detail: null });
            _el.main.dispatchEvent(event); // dispatch created event on the main element
        }

        // @name _onInlineVideoPlay
        // @desc listener function for on play event for the inline video
        function _onInlineVideoPlay() {
            _isPlaying = true; // set the video play flag and create a play event
            var event = new CustomEvent("inline-video.onPlay", { detail: null });
            _el.main.dispatchEvent(event); // dispatch created event on the main element
        }

        // @name _onInlineVideoStop
        // @desc listener function for on stop event for the inline video
        function _onInlineVideoStop() {
            _isPlaying = false; // reset the video play flag and create a stop event
            var event = new CustomEvent("inline-video.onStop", { detail: null });
            _el.main.dispatchEvent(event); // dispatch created event on the main element
        }

        // @name _addElementToDOM
        // @desc function to add the given element to the main DOM element
        // @param {DOM} element - the element that needs to be added to the DOM
        function _addElementToDOM(element) {
            // add the element to the main element 
            // if a valid parent node does not exist
            var parent = element.parentNode;
            if(!parent || !parent.nodeName || !parent.nodeType) {
                _el.main.appendChild(element);
            }
        }

        // @name _removeElementFromDOM
        // @desc function to remove the given element from the main DOM element
        // @param {DOM} element - the element that needs to be removed from the DOM
        function _removeElementFromDOM(element) {
            // remove the element from the main element 
            // if a valid parent node does exist
            var parent = element.parentNode;
            if(parent && parent.nodeName && parent.nodeType) {
                _el.main.removeChild(element);
            }
        }

        // @name _onVideoLoad
        // @desc listener function for on load event for the video
        function _onVideoLoad(event) {
            _removeOnVideoLoad(); // remove the on load event listener for video
            _onInlineVideoLoad(); // dispatch on load event for the inline video
        }

        // @name _addOnVideoLoad
        // @desc function to add on load event listener to video
        function _addOnVideoLoad() { 
            _el.video.addEventListener("canplay", _onVideoLoad);
        }

        // @name _removeOnVideoLoad
        // @desc function to remove on load event listener from video
        function _removeOnVideoLoad() { 
            _el.video.removeEventListener("canplay", _onVideoLoad);
        }

        // @name _playVideo
        // @desc function to play the <video> video
        function _playVideo() {
            if(_el.video) { 
                // if the flag to remove the video 
                // from the DOM on stop has been set
                if(_removeVideoOnStop) {
                    // add the element to the DOM
                    _addElementToDOM(_el.video);
                } 

                // play the video if a source exists
                var source = _el.video.getAttribute("src");
                if(source && source.length) { 
                    // play the video
                    _el.video.play();
                }

                // else set the video source and 
                // play the video once it is set
                else {
                    // remove previous on load event listener
                    // and add a new on load event listener
                    _removeOnVideoLoad(); _addOnVideoLoad();

                    source = _el.video.getAttribute("data-src");
                    _el.video.setAttribute("src", source);
                    _el.video.removeAttribute("data-src");
                    _el.video.volume = 0; // mute video
                    _el.video.play(); // play the video
                } 
            }
        }

        // @name _playVideo
        // @desc function to stop the <video> video
        function _stopVideo() {
            if(_el.video) { 
                // stop the video if a source exists
                var source = _el.video.getAttribute("src");
                if(source && source.length) { 
                    // stop the video
                    _el.video.pause();
                }

                // if the flag to remove the video 
                // from the DOM on stop has been set
                if(_removeVideoOnStop) {
                    // remove the element from the DOM
                    _removeElementFromDOM(_el.video);
                }
            }
        }

        // @name _onCanvasLoad
        // @desc listener function for on load event for the canvas
        function _onCanvasLoad(event) {
            _whitewater.player.play(); // play the whitewater video with the player
            _whitewater.isLoading = false; // set whitewater loading flag as false
            _whitewater.hasLoaded = true; // set whitewater has loaded flag as true
            _removeOnCanvasLoad(); // remove the on load event listener for canvas
            _onInlineVideoLoad();  // dispatch on load event for the inline video
        }

        // @name _addOnCanvasLoad
        // @desc function to add on load event listener to canvas
        function _addOnCanvasLoad() { 
            _el.canvas.addEventListener("whitewaterload", _onCanvasLoad);
        }

        // @name _removeOnCanvasLoad
        // @desc function to remove on load event listener from canvas
        function _removeOnCanvasLoad() { 
            _el.canvas.removeEventListener("whitewaterload", _onCanvasLoad);
        }

        // @name _playCanvas
        // @desc function to play the <canvas> video
        function _playCanvas() {
            if(_el.canvas) {
                // if the flag to remove the canvas 
                // from the DOM on stop has been set
                if(_removeCanvasOnStop) {
                    // add the element to the DOM
                    _addElementToDOM(_el.canvas);
                } 

                // play the video if player exists
                // and the video assets have loaded
                if(_whitewater.player != null
                    && _whitewater.hasLoaded) {
                    _whitewater.player.play();
                } 

                // add listener if the video is loading
                else if(_whitewater.player != null
                        && _whitewater.isLoading) {
                    // remove previous on load event listener
                    // and add a new on load event listener
                    _removeOnCanvasLoad(); _addOnCanvasLoad();
                }

                // else set the video source, create a new 
                // whitewater player and play once it is created
                else {
                    var source = _el.canvas.getAttribute("data-src"); // get the video source
                    _whitewater.player = new Whitewater(_el.canvas, source, _whitewater.options);
                    _el.canvas.removeAttribute("data-src"); // remove the attribute once set

                    _whitewater.isLoading = true; // set whitewater loading flag as true
                    _whitewater.hasLoaded = false; // set whitewater has loaded flag as true

                    // create a new whitewater player and add a load event listener
                    _el.canvas.addEventListener("whitewaterload", _onCanvasLoad);
                }
            }
        }

        // @name _stopCanvas
        // @desc function to play the <canvas> video
        function _stopCanvas() {
            if(_el.canvas) {
                // stop the video if player exists
                // and the video assets have loaded
                if(_whitewater.player != null
                    && _whitewater.hasLoaded) {
                    _whitewater.player.pause();
                }

                // if the flag to remove the canvas 
                // from the DOM on stop has been set
                if(_removeCanvasOnStop) {
                    // remove the element from the DOM
                    _removeElementFromDOM(_el.canvas);
                }
            }
        }

        // @name _onGifLoad
        // @desc listener function for on load event for the gif
        function _onGifLoad(event) {
            _removeOnGifLoad(); // remove the on load event listener for gif
            _onInlineVideoLoad(); // dispatch on load event for the inline gif
        }

        // @name _addOnGifLoad
        // @desc function to add on load event listener to gif
        function _addOnGifLoad() { 
            _el.gif.addEventListener("load", _onGifLoad);
        }

        // @name _removeOnGifLoad
        // @desc function to remove on load event listener from gif
        function _removeOnGifLoad() { 
            _el.gif.removeEventListener("load", _onGifLoad);
        }

        // @name _playGif
        // @desc function to play the <img> gif video
        function _playGif() {
            if(_el.gif) { 
                // add the element to the DOM
                _addElementToDOM(_el.gif);

                // play the gif if a source exists
                var source = _el.gif.getAttribute("src");
                if(source && source.length) {
                    /* no play required */
                }

                // else set the gif source and 
                // play the gif once it is set
                else {
                    // remove previous on load event listener
                    // and add a new on load event listener
                    _removeOnGifLoad(); _addOnGifLoad();

                    source = _el.gif.getAttribute("data-src");
                    _el.gif.setAttribute("src", source);
                    _el.gif.removeAttribute("data-src");
                    /* no play required */
                } 
            }
        }

        // @name _stopGif
        // @desc function to play the <img> gif video
        function _stopGif() {
            if(_el.gif) { 
                // stop the gif if a source exists
                var source = _el.gif.getAttribute("src");
                if(source && source.length) { 
                    /* no stop required */
                }

                // remove the element from the DOM
                _removeElementFromDOM(_el.gif);
            }
        }

        // ---------------------------------------------
        //   Public methods
        // ---------------------------------------------
        // @name isReady, isLocked, isPlaying, hasLoaded
        // @desc functions to check if the video is ready, locked, playing (and) loaded
        // @return {Boolean} - returns true / false if video is ready, locked, playing (and) loaded
        function isReady()   { return _isReady;   }
        function isLocked()  { return _isLocked;  }
        function isPlaying() { return _isPlaying; }
        function hasLoaded() { return _hasLoaded; }

        // @name lock
        // @desc function to lock the video ( video cannot be played )
        function lock() {
            // only proceed if the video
            // is currenly not locked
            if(!_isLocked) { 
                _isLocked = true;
                console.log("INLINE VIDEO MESSAGE: The inline video is now locked and cannot be played.");
            }
        }

        // @name unlock
        // @desc function to unlock the video ( video can be played )
        function unlock() {
            // only proceed if the video
            // is currenly already locked
            if(_isLocked) { 
                _isLocked = false;
                console.log("INLINE VIDEO MESSAGE: The inline video is now unlocked and can be played.");
            }
        }

        // @name play
        // @desc functions to play the inline video
        function play() {
            // only proceed if the video
            // is currenly not playing 
            // and has not been locked
            if(!_isPlaying && !_isLocked) {
                if(_el.video) { _playVideo(); }
                else if(_el.gif) { _playGif(); }
                else if(_el.canvas) { _playCanvas(); }

                // dispatch the video play event
                _onInlineVideoPlay();
            }
        }

        // @name stop
        // @desc functions to stop the inline video
        function stop() {
            // only proceed if the video
            // is currenly already playing
            if(_isPlaying) {
                if(_el.video) { _stopVideo(); }
                else if(_el.gif) { _stopGif(); }
                else if(_el.canvas) { _stopCanvas(); }

                 // dispatch the video stop event
                _onInlineVideoStop();
            }
        }

        // ---------------------------------------------
        //   Constructor block
        // ---------------------------------------------
        // check if the component has valid options
        // element - should be a valid DOM element
        if(!options || !options.element 
            || !options.element.nodeName 
            || !options.element.nodeType) {
            console.log("INLINE VIDEO ERROR: Cannot create inline video with invalid options.");
            return null;  // return null if invalid
        }

        // check if the component has valid options
        // source  - should contain valid folder paths for videos
        //           source.desktop - folder path for desktop video
        //           source.tablet  - folder path for tablet video
        //           source.mobile  - folder path for mobile video
        if(!options || !options.source
            || !options.source.desktop || !options.source.desktop.length
            || !options.source.tablet  || !options.source.tablet.length
            || !options.source.mobile  || !options.source.mobile.length) {
            console.log("INLINE VIDEO ERROR: Cannot create inline video with invalid options.");
            return null;  // return null if invalid
        }

        // check if flags to indicate if the video (and) canvas
        // should be removed from the DOM when stopped have been set
        if(options.removeVideoOnStop == true) { _removeVideoOnStop = true;  }
        if(options.removeCanvasOnStop == true) { _removeCanvasOnStop = true; }

        // get the main DOM element and
        // add the default element class
        _el.main = options.element;
        _el.main.classList.add(_class.main);

        // create all the image elements 
        // and the parent layout wrapper
        _el.image.desktop = _createImage("desktop");
        _el.image.tablet  = _createImage("tablet");
        _el.image.mobile  = _createImage("mobile");
        _el.layout = _createLayout();

        // check if the device is a mobile
        if(CONFIG.device.isPhone) {
            if(CONFIG.device.isAndroidOld 
                || CONFIG.device.isIOSOld) { 
                _el.gif = _createGif("mobile");  } // create gif for older mobiles
            else { _el.canvas = _createCanvas("mobile"); } // create canvas for newer mobiles
        }

        // else check if the device is a tablet
        else if(CONFIG.device.isTablet) {
            if(CONFIG.device.isAndroidOld 
                || CONFIG.device.isIOSOld) { 
                _el.gif = _createGif("tablet");  } // create gif for older tablets
            else { _el.canvas = _createCanvas("tablet"); } // create canvas for newer tablets
        }

        // else the default device is a desktop
        else { _el.video = _createVideo("desktop"); } // create video for desktop

        // clear out any previous markup
        // within the main DOM element
        _el.main.innerHtml = "";

        // append all the image elements
        // and the parent layout wrapper
        _el.layout.appendChild(_el.image.desktop);
        _el.layout.appendChild(_el.image.tablet);
        _el.layout.appendChild(_el.image.mobile);
        _el.main.appendChild(_el.layout);

        // append the canvas, video and gif elements
        // ( note: elements are also appended during play )
        if(_el.canvas && !_removeCanvasOnStop) { _el.main.appendChild(_el.canvas); }
        if(_el.video && !_removeVideoOnStop)   { _el.main.appendChild(_el.video);  }

        // dispatch the video ready event
        setTimeout(_onInlineVideoReady, CONFIG.timeout.scope);

        // ---------------------------------------------
        //   Instance block
        // ---------------------------------------------
        return {
            play: play, // function to play the inline video
            stop: stop, // function to stop the inline video

            lock: lock, // function to lock the inline video ( video can be played )
            unlock: unlock, // function to unlock the inline video ( video cannot be played )

            isReady  : isReady,   // function to check if the video is ready
            isLocked : isLocked,  // function to check if the video is locked
            isPlaying: isPlaying, // function to check if the video is playing
            hasLoaded: hasLoaded  // function to check if the video has loaded
        };
    }

    // ---------------------------------------------
    //   Module export block
    // ---------------------------------------------
    module.exports = InlineVideo;

})();

