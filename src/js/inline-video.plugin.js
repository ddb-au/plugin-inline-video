"use strict";

// -------------------------------------
//   Dependencies
// -------------------------------------
/** 
  * @plugins
  * require("mobile-detect");
**/

// base
require("./base/raf");
require("./base/event");
require("./base/query");

// components
var InlineVideo = require("./components/inline-video.component");
window.InlineVideo = InlineVideo; // set component in global namespace

// -------------------------------------
//   Inline Video Plugin
// -------------------------------------
/** 
  * @name inline-video.plugin
  * @desc The main js file that contains the run 
          options and config options for the app.
**/
(function() {
    var CONFIG = require("./inline-video.config"); // config
    console.log("inline-video.plugin.js loaded."); // loaded

    // function to check if the given element is in view
    function isElementInView(element) {
        var classList = element.classList;
        var style = getComputedStyle(element);
        var rect = element.getBoundingClientRect();

        return (
            (style["display"] != "none") &&
            rect.left >= (0 - (rect.width / 2)) &&
            rect.right <= ((window.innerWidth || _elHtml.clientWidth) + (rect.width / 2)) &&

            rect.top >= (0 - (rect.height / 2)) &&
            rect.bottom <= ((window.innerHeight || _elHtml.clientHeight) + (rect.height / 2))
        );
    }

    // get the inline test video elements
    var elements = query(".inline-video--test");
    var components = []; // create array for the videos

    // only proceed if test video elements exist
    elements.forEach( function(element, index) {
        // create a new reference for 
        // the inline video component
        var video = null;

        // add an inline video on ready event listener to the element
        // ( note: this needs to be attached before the inline video component is created )
        element.addEventListener("inline-video.onReady", function(event) {
            console.log("inline-video.onReady callback triggered.");
            console.log("video.isReady(): " + video.isReady());
            console.log(event);
        });

        // add an inline video on load event listener to the element
        // ( note: the video is loaded only after video.play() is called for the first time )
        element.addEventListener("inline-video.onLoad", function(event) {
            console.log("inline-video.onLoad callback triggered.");
            console.log("video.hasLoaded(): " + video.hasLoaded());
            console.log(event);
        });

        // initiate the inline video component
        video = new InlineVideo({
            element: element, // set the component element

            source: { // set the component source folder
                desktop: CONFIG.path.videos + "desktop/", // desktop
                tablet:  CONFIG.path.videos + "tablet/",  // tablet
                mobile:  CONFIG.path.videos + "mobile/"   // mobile
            },

            // flags to indicate if the video (and) canvas
            // should be removed from the DOM when stopped
            removeVideoOnStop: false, // default flag value
            removeCanvasOnStop: false // default flag value
        });

        // add an inline video on play event listener to the element
        element.addEventListener("inline-video.onPlay", function(event) {
            console.log("inline-video.onPlay callback triggered.");
            console.log("video.isPlaying(): " + video.isPlaying());
            console.log(event);
        });

        // add an inline video on stop event listener to the element
        element.addEventListener("inline-video.onStop", function(event) {
            console.log("inline-video.onStop callback triggered.");
            console.log("video.isPlaying(): " + video.isPlaying());
            console.log(event);
        });

        // push the video and the element into array
        components.push({ element: element, video: video });

    });

    // check if there are test videos
    if(components && components.length) {
        // attach video play and stop to the window scroll
        window.addEventListener("scroll", function() {

            // loop through all the video components
            components.forEach(function(component, index) {
                var video = component.video; // get component video
                var element = component.element; // get component element

                // check if the element is in view
                if(isElementInView(element)) {
                    // play the video if the element
                    // is in view and is not playing
                    if(!video.isPlaying()) {
                        /* video.unlock(); */
                        video.play();
                    }
                } else {
                    // stop the video if the element
                    // is in not view and is playing
                    if(video.isPlaying()) { 
                        /* video.lock(); */
                        video.stop();
                    }
                }
            });
            
        });

        // trigger window scroll once on load
        setTimeout(function() { // with a small timeout
            var event = new CustomEvent("scroll"); // create event
            window.dispatchEvent(event); // dispatch created event
        }, CONFIG.timeout.scope);
    }


})();