## Plugin Inline Video
~~~~
NAME: plugin-inline-video
AUTHOR: Pradeep Sekar

REPO: https://bitbucket.org/tddbsydney/plugin-inline-video/
STAGING: Not available
PRODUCTION: Not available
~~~~

##
----
## DEVELOPMENT
##
----
##

## Installing Node, Bower and Gulp

Install the latest **LTS** version of node which can be found [here](https://nodejs.org/en/). ( which was **6.9.1** at the time of writing this document ). Once node in installed on your machine, open the terminal (or) command prompt and run **npm install -g bower** to install bower and then run **npm install -g gulp** to install gulp.

## Commands to Run

Open the terminal (or) command prompt at the root folder, and run **npm install**, followed by **bower install**. Once installation of both the required node and bower packages are complete, run **gulp**. You can optionally also use **gulp dev** or **gulp prod** to indicate development (or) production mode. The default mode is development, which injects unminifed source files from **/web/src/** into **index.html**. The production mode will use minified files from **/web/dist/**, and also strip out comments, and console statements.

## Commands to Deploy

Open the terminal (or) command prompt at the root folder and run **gulp deploy**. The compiled files will be moved and locally deployed to **/web/build/**. All remaining tasks will also be completed and it's watchers removed. You can optionally use **gulp deploy:dev** (or) **gulp deploy:prod** to deploy the files in development or production mode within deployment mode. The default mode is production. This will also prevent the watchers from being removed, so as you can continue to work on the files within deployment mode.
